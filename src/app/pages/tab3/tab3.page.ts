import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { Favorite, Result, ResultComics } from 'src/app/interfaces';
import { FavoritosService } from 'src/app/services/favoritos.service';


@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit{

  public categories: string[] = ["characters","comics"];
  public selectedCategory = this.categories[0];
  public information: any[] = [];
  operations: any;
  

  constructor(private favoritesService:FavoritosService) {}

  segmentChanged(event:any){
    this.selectedCategory = event.detail.value;
    this.information = [];
    this.information=this.favoritesService.getInfoOnFavoritePage(this.selectedCategory);
  }

  ngOnInit(): void {
    this.favoritesService.manageCharactersList();
    this.favoritesService.manageComicsList();
    this.information=this.favoritesService.getInfoOnFavoritePage(this.selectedCategory);
  }

  doRefresh(event) {
    

    setTimeout(() => {
      this.ngOnInit();
      event.target.complete();
    }, 1500);
  }
}
