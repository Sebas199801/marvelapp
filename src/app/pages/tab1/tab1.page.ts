import { Component, EventEmitter, OnInit, ViewChild } from '@angular/core';
import { IonInfiniteScroll } from '@ionic/angular';
import { Result } from 'src/app/interfaces';
import { FavoritosService } from 'src/app/services/favoritos.service';
import { PersonajesService } from 'src/app/services/personajes.service';


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page  implements OnInit{

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  

  constructor(private charactersService : PersonajesService, private favoritesServices: FavoritosService) {}

  characters: Result[] = [];



  ngOnInit(): void {
      this.charactersService.getCharacters().subscribe(characters => this.characters=[...characters])
      this.favoritesServices.manageCharactersList();

  }

  loadData(){
    this.charactersService.getCharacters(true).subscribe(personajes => this.characters.push(...personajes))
    this.infiniteScroll.complete();
  }
  doRefresh(event) {
    

    setTimeout(() => {
      this.ngOnInit();
      event.target.complete();
    }, 1500);
  }
}
