import { Component, OnInit, ViewChild } from '@angular/core';
import { IonInfiniteScroll } from '@ionic/angular';
import { FavoritosService } from 'src/app/services/favoritos.service';
import { PersonajesService } from 'src/app/services/personajes.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {

  constructor(private charactersService:PersonajesService, private favoritesService: FavoritosService) {}

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;


  public comics = [];

  getComics(){
    this.charactersService.getComics().subscribe(res=> this.comics=[...res])
  }

  ngOnInit(): void {
      this.getComics();
      this.favoritesService.manageComicsList();
  }

  loadData(){
    this.charactersService.getComics(true).subscribe(res=>this.comics.push(...res))
    this.infiniteScroll.complete();
  }

  doRefresh(event) {
    

    setTimeout(() => {
      this.ngOnInit();
      event.target.complete();
    }, 2000);
  }

}
