import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TarjetasComponent } from './tarjetas/tarjetas.component';
import { TarjetaComponent } from './tarjeta/tarjeta.component';





@NgModule({
  declarations: [TarjetasComponent, TarjetaComponent],
  imports: [
    CommonModule
  ],
  exports:[TarjetasComponent]
})
export class ComponentsModule { }
