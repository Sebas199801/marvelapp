import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Result} from 'src/app/interfaces';
import { PersonajesService } from 'src/app/services/personajes.service';
import { InAppBrowser } from '@awesome-cordova-plugins/in-app-browser/ngx';
import { Platform } from '@ionic/angular';
import { FavoritosService } from 'src/app/services/favoritos.service';
import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';
@Component({
  selector: 'app-tarjeta',
  templateUrl: './tarjeta.component.html',
  styleUrls: ['./tarjeta.component.scss'],
})
export class TarjetaComponent implements OnInit {
  @Input() character: Result;
  public favorite: boolean = false;
  constructor(
    private platform: Platform,
    private iab: InAppBrowser,
    private router: Router,
    private personajesService: PersonajesService,
    private faveoriteService: FavoritosService,
    private shareService: SocialSharing
  ) {}

  public src = "";

  ngOnInit() {
    this.favorite = this.faveoriteService.searchFavorite(this.character);
    //this.src ="https://i.annihil.us/u/prod/marvel/i/mg/6/20/52602f21f29ec/standard_amazing.jpg"
    this.src = `${this.character.thumbnail.path}/standard_amazing.${this.character.thumbnail.extension}`;
  }

  openCharacter() {
     if (this.platform.is('ios') || this.platform.is('android')) {
      const browser = this.iab.create(this.character.urls[0].url);
      browser.show();
      return;
     }
     window.open(this.character.urls[0].url);
      
  }

  deleteFromFavorites() {
    this.favorite = false;
    this.faveoriteService.deleteFromFavorites(this.character);
    this.faveoriteService.getInfoOnFavoritePage();
  }

  addToFavorites() {
    this.favorite = true;
    if (this.character.name) {
      this.faveoriteService.saveCharacterOnFavorites(this.character);
      this.faveoriteService.getInfoOnFavoritePage();
      
    } else {
      this.faveoriteService.saveComicOnFavorites(this.character);
      this.faveoriteService.getInfoOnFavoritePage();
      
    }
  }

  share(){
    this.shareService.share(null,null,null,this.character.urls[0].url);
  }
}
