import { Component, Input, OnInit } from '@angular/core';
import { Result, ResultComics } from 'src/app/interfaces';

@Component({
  selector: 'app-tarjetas',
  templateUrl: './tarjetas.component.html',
  styleUrls: ['./tarjetas.component.scss'],
})
export class TarjetasComponent implements OnInit {

  @Input() characters: Result[]|ResultComics[]= [];

  constructor() { }

  ngOnInit() {}

}
