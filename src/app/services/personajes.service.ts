import {  Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Md5 } from 'ts-md5/dist/md5';
import { characterResult} from '../interfaces';
import { map } from 'rxjs/operators';



const privateKey = environment.privateApiKey;
const publicKey = environment.publicApiKey;
const ts = Date.now();
const apiUrl = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class PersonajesService {
  
  // limite de personas para controlar la cantidad de registros que llegan de la peticion http:/
  // this is the limit of characters, with this you can control how many registers come from the http:// petition
  private charactersLimit = 0;
  private comicsLimit = 0;
  

  constructor(public http: HttpClient) { }

  private executeQuery<T>(endpoint: string) {
    return this.http.get<T>(`${apiUrl}${endpoint}`)
  }
  // api key para la gestion de la peticion http que solicita el api.
  // api key.
  getApiKey(): string {
    const hash = Md5.hashStr(`${ts}${privateKey}${publicKey}`);
    return `ts=${ts}&apikey=${publicKey}&hash=${hash}`
  }
  // solicitud de personajes, tanto para la primera vez como para el uso del infinite scroll.
  // Characters request, for the first time or to use it on the infinite scroll
  getCharacters(loadMore: boolean = false) {
    if (loadMore) {
      this.charactersLimit += 20;
      return this.executeQuery<characterResult>(`/characters?limit=20&offset=${this.charactersLimit}&${this.getApiKey()}`)
        .pipe(map(res => res.data.results))
    }
    return this.executeQuery<characterResult>(`/characters?${this.getApiKey()}`)
      .pipe(map(res => res.data.results))
  }
  // solicitud de comics, tanto para la primera vez como para el uso del infinite scroll.
  // Comics request, for the first time or to use it on the infinite scroll
  getComics(loadMore: boolean = false) {
    if (loadMore) {
      this.comicsLimit += 10;
      return this.executeQuery<characterResult>(`/comics?limit=10&offset=${this.comicsLimit}&${this.getApiKey()}`)
        .pipe(map(res => res.data.results))
    }
    return this.executeQuery<characterResult>(`/comics?${this.getApiKey()}`)
      .pipe(map(res => res.data.results))
  }
  
}
