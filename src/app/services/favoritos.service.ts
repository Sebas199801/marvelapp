import { Injectable } from '@angular/core';
import { Favorite, Result, ResultComics } from '../interfaces';
import { ToastService } from './toast.service';

@Injectable({
  providedIn: 'root',
})
export class FavoritosService {
  constructor(private toast:ToastService) {}
  // variable to handle the storage
  private storage = window.localStorage;
  // lists to be saved in the localStorage for use on the favorites page.
  public favoriteCharactersList: Favorite[] = [];
  public favoriteComicsList: Favorite[] = [];
  // variable to keep the category where are the favorites. 
  public category:string;
  // With this method you can save the characters on the favorites list
  // Este metodo se usa para guardar en la lista de favoritos los characters
  saveCharacterOnFavorites(character: Result) {
    let found = this.searchCharacter(character);
    if (!found) {
      let favorite: Favorite = {
        character: character,
        favorite: true,
      };
      this.favoriteCharactersList.push(favorite);
      this.saveOnStorage();
      this.toast.presentToast('Personaje agregado a favoritos')
    }
  }
  //and the comics with this method
  saveComicOnFavorites(comic: any) {
    let found = this.searchComic(comic);
    if (!found) {
      let favorite: Favorite = {
        character: comic,
        favorite: true,
      };
      this.favoriteComicsList.push(favorite);
      this.saveComicOnStorage();
      this.toast.presentToast('Comic agregado a favoritos')
    }
  }

  // eliminacion de un character de la lista de favoritos
  // delete a character from the favorites list
  deleteFromFavorites(character:any) {
    if(character.name){
      for(let i=0; i<this.favoriteCharactersList.length; i++){
        if(this.favoriteCharactersList[i].character.id === character.id){
          this.favoriteCharactersList.splice(i,1);
          this.saveOnStorage();
          this.manageCharactersList();
          this.toast.presentToast('Personaje eliminado de favoritos')
        }
      }
    }else{
      for(let i=0; i<this.favoriteComicsList.length; i++){
        if(this.favoriteComicsList[i].character.id === character.id){
          this.favoriteComicsList.splice(i,1);
          this.saveComicOnStorage();
          this.manageComicsList();
          this.toast.presentToast('Comic eliminado de favoritos')
        }
      }
    }
  }
  
  saveOnStorage() {
    this.storage.setItem(
      'characters',
      JSON.stringify(this.favoriteCharactersList)
    );
  }

  saveComicOnStorage() {
    this.storage.setItem('comics', JSON.stringify(this.favoriteComicsList));
  }
  // trae las listas del local storage y las pone en las listas locales
  // fetches the lists from the local storage and puts them on the local lists
  manageLists(){
    this.manageComicsList();
    this.manageCharactersList();
  }
    // reload the characters list and it update with the local storage
  manageCharactersList() {
    if (this.storage.getItem('characters')) {
      this.favoriteCharactersList = JSON.parse(
        this.storage.getItem('characters')
      );

    }
  }
// reload the comics list and it update with the local storage
  manageComicsList() {
    if (this.storage.getItem('comics')) {
      this.favoriteComicsList = JSON.parse(
        this.storage.getItem('comics')
      );

    }
  }

  searchCharacter(character: Result): boolean {
    for (let fav of this.favoriteCharactersList) {
      if (fav.character.id === character.id) {
        return true;
      }
    }
    return false;
  }

  searchComic(comic: ResultComics) {
    for (let fav of this.favoriteComicsList) {
      if (fav.character.id === comic.id) {
        return true;
      }
    }
    return false;
  }

  // metodo para verificar si el character o el comic esta marcado como favorite.
  // with this method you can verify if the character o the comic is marked as favorite
  searchFavorite(favorite:any):boolean{
  
    if(favorite.name){
      if(this.searchCharacter(favorite)){
  
        return true;
      }
    }else{
     if(this.searchComic(favorite)){
       return true;
     }
    }
    return false;
  }

  getInfoOnFavoritePage(category?:string){
    this.category = category
    let list:Favorite[];
    let information:any[]=[];
    if(category==="characters"){
      list = this.favoriteCharactersList;
    }
    else{
      list= this.favoriteComicsList;
    }

    list.forEach((item)=>{
      information.push(item.character);
    })
    return information;
  }


}
